$(function() {

    $("#txt_date_start").flatpickr({
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
        defaultDate: "today",
    });

    $("#txt_date_finish").flatpickr({
        altInput: true,
        altFormat: "j F Y",
        dateFormat: "Y-m-d",
        defaultDate: "today",
    });

    var todoTable = $('#todo-list-table').DataTable({
        processing: true,
        autoWidth: false,
        ajax: {
            url: '/todo?get=data',
            type: 'GET',
            dataSrc: ''
        },
        columns: [
            {
                data: 'title'
            },
            {
                data: 'description'
            },
            {
                data: 'date_start',
                render: function ( data, type, row, meta ) {
                    var tanggal = moment(data).format("DD MMM YYYY");
                    return tanggal;
                }
            },
            {
                data: 'date_finish',
                render: function ( data, type, row, meta ) {
                    var tanggal = moment(data).format("DD MMM YYYY");
                    return tanggal;
                }
            },
            {
                data: 'status',
                render: function ( data, type, row, meta ) {
                    if(data == 1){
                        rownya = 'Achieved';
                    } else if (data == 2) {
                        rownya = 'Pending';
                    } else {
                        rownya = 'Expired'
                    }

                    return rownya;
                }
            },
            {
                data: 'id',
                className: 'text-center',
                render: function ( data, type, row, meta ) {
                    var edit = '';
                    var del = '';

                    // if(canDeleteSurveyConst == 1){
                        del = '<button class="todo-delete btn btn-sm btn-danger" title="Delete">Delete</button>';
                    // }

                    return del;
                }
            }
        ],
        columnDefs: [
            {
                orderable: true, targets: '_all'
            }
        ],
    });

    $('#save-todo').on('click', function (e) {
        // Parsley initialize option
        var parsTodo = $('#form-todo-list').parsley({
            trigger: 'change',
            successClass: "has-success",
            errorClass: "has-error",
            classHandler: function (el) {
                return el.$element.closest('.form-group'); //working
            },
            errorsWrapper: '<div class="invalid-message"></div>',
            errorTemplate: '<span></span>',
        });

        if( !parsTodo.validate() ){
            e.preventDefault();
        } else {

            var data = {
                title : $('[name=txt_title]').val(),
                description : $('[name=txt_desc]').val(),
                date_start: $('[name=txt_date_start]').val(),
                date_finish: $('[name=txt_date_finish]').val(),
                status: $('[name=sel_status]').val()
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/todo',
                data: data,
                success: function(msg) {
                    $('#todo-form').modal('hide');

                    if(msg[0].status == 'success'){
                        Swal.fire({
                            type: msg[0].status,
                            title: msg[0].title,
                            text: msg[0].message,
                            onAfterClose: function (x){

                                $('#todo-list-table').DataTable().ajax.reload();

                            }
                        });

                    } else {
                        Swal.fire({
                            type: msg[0].status,
                            title: msg[0].title,
                            text: msg[0].message,
                        });
                    }
                },
                error: function(msg) {
                    Swal.fire({
                        type: msg[0].status,
                        title: msg[0].title,
                        text: 'Error from backend : ' + msg,
                    });
                }
            });

        }
    });

    // Delete
    $('#todo-list-table tbody').on( 'click', 'button.todo-delete', function () {

        var tr = $(this).closest('tr');
        var rownya = todoTable.row( tr );
        var datanya = rownya.data();
        var index = datanya.id;
        var url = '/todo/' + index;

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
            }).then((result) => {
            if (result.value) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'DELETE',
                    url: url,
                    data: index,
                    success: function(msg) {
                        todoTable.ajax.reload();
                        todoTable.rows().invalidate().draw();
                        if(msg[0].status == 'success'){
                            Swal.fire({
                                type: msg[0].status,
                                title: msg[0].title,
                                text: msg[0].message,
                            });
                            todoTable.ajax.reload();
                            todoTable.rows().invalidate().draw();
                        } else {
                            Swal.fire({
                                type: msg[0].status,
                                title: msg[0].title,
                                text: msg[0].message,
                            });
                        }
                    },
                    error: function(msg) {
                        Swal.fire({
                            type: msg[0].status,
                            title: msg[0].title,
                            text: 'Error from backend : ' + msg,
                        });
                    }
                });

            }
        });
    } );

});
