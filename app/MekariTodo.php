<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MekariTodo extends Model
{
    // Set table name
    protected $table = 'todo_lists';

    protected $primaryKey = 'id';
    protected $fillable = [
        'title', 'description', 'date_start', 'date_finish', 'status', 'owner_id'
    ];

    public $timestamps = false;


}
