<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MekariTodo;

class MekariTodoController extends Controller
{
    // index
    public function index(Request $request)
    {

        if($request->get == 'data')
        {

            $data = MekariTodo::all();

            return $data;

        }

        return view('todo');

    }

    public function store(Request $request)
    {

        try {

            $initStore = new MekariTodo;

            $initStore->owner_id = \Auth::user()->id;
            $initStore->title = $request->title;
            $initStore->description = $request->description;
            $initStore->date_start = $request->date_start;
            $initStore->date_finish = $request->date_finish;
            $initStore->status = $request->status;


            $store = $initStore->save();

            $message = [
                    [
                        'status' => 'success',
                        'title' => 'Create Todo List',
                        'message' => 'Create Todo List data success.',
                        'id' => $initStore->id
                    ]
                ];

            return $message;

        } catch (\Exception $e) {

            $message = [
                [
                    'status' => 'error',
                    'title' => 'Create Todo List',
                    'message' => $e,
                    'id' => 000
                ]
            ];

            return $message;

        }

    }

    public function destroy(Request $request, $id)
    {

        try {

            $initStore = MekariTodo::find($id);

            $store = $initStore->delete();

            $message = [
                [
                    'status' => 'success',
                    'title' => 'Delete Todo List',
                    'message' => 'Success delete Todo List',
                    'id' => 000
                ]
            ];

            return $message;

        } catch (\Exception $e) {

            $message = [
                [
                    'status' => 'error',
                    'title' => 'Delete Todo List',
                    'message' => $e,
                    'id' => 000
                ]
            ];

            return $message;

        }

    }
}
