@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            TODO LISTS
                        </div>
                        <div class="col-6 text-right">
                            <button type="button" name="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#todo-form">Add New</button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table id="todo-list-table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th style="width: 20%;">Title</th>
                                <th style="width: 25%;">Description</th>
                                <th style="width: 15%;">Date Start</th>
                                <th style="width: 15%;">Date Finish</th>
                                <th style="width: 15%;">Status</th>
                                <th style="width: 15%;">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="todo-form" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Todo Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="form-todo-list">
            <div class="form-group">
              <label>Title</label>
              <input type="text" class="form-control" name="txt_title" id="txt_title" placeholder="Enter title" required>
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea type="text" class="form-control" name="txt_desc" id="txt_desc" placeholder="Enter Description"></textarea>
            </div>
            <div class="form-group">
              <label>Date Start</label>
              <input type="text" class="form-control" name="txt_date_start" id="txt_date_start" required>
            </div>
            <div class="form-group">
              <label>Date Finsih</label>
              <input type="text" class="form-control" name="txt_date_finish" id="txt_date_finish" required>
            </div>
            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="sel_status" id="sel_status">
                  <option value="1">Achieved</option>
                  <option value="2">Pending</option>
                  <option value="3">Expired</option>
              </select>
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-todo">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('todo.js') }}" charset="utf-8"></script>
@endsection
